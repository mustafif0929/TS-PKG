COPY ts-pkg.deb /bin/
RUN dpkg -i /bin/ts-pkg.deb
RUN apt-get update
RUN apt-get install libc6
RUN apt-get install build-essential
RUN apt-get install -y glibc-doc
RUN apt-get install -f