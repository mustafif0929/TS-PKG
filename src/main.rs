use std::process::Command;
use structopt::StructOpt;
mod lib;
use lib::*;
#[derive(StructOpt)]
#[structopt(name = "Tomorrow-Study Package Manager")]

struct Cli {
    #[structopt(subcommand)]
    cmd: Cmd,
}
#[derive(StructOpt)]
enum Cmd {
    #[structopt(name = "get", about = "Gets content directly to System")]
    Get(Get),
    #[structopt(name = "list", about = "Lists out available Content")]
    List(List),
}
#[derive(StructOpt)]
struct Get {
    id: String,
}
#[derive(StructOpt)]
struct List {
    #[structopt(long = "fs")]
    #[structopt(about="Prints all formula Sheets")]
    fs: bool,
    #[structopt(long = "basic", about="Prints all Basic Books Content")]
    basic: bool,
    #[structopt(long = "all", about="Prints all available Content")]
    all: bool,
}

// Content Template
/*
Content{
        name: String::from(""),
        author: String::from(""),
        id: String::from(""),
        url: String::from(""),
};

*/

/*Ex. $ ts <cmd> <tag>*/

fn main() {
    let mut advMK = Content {
        name: String::from("Advance Functions (FS)"),
        author: String::from("Mustafif Khan"),
        id: String::from("AMK"),
        url: String::from(
            "https://github.com/MKProj/TomorrowStudy/raw/V1.0.0/tmp/AdvFuncMK/AdvFuncMK.pdf",
        ),
    };

    let mut calc1MK = Content{
        name: String::from("Calculus 1 (FS)"),
        author: String::from("Mustafif Khan"),
        id: String::from("C1MK"),
        url: String::from(
            "https://github.com/MKProj/TomorrowStudy/raw/V1.0.0/Library/Formula_Sheets/Calc1MK/Calc1MK.pdf"
        ),
};
    let mut fs = Category {
        cont: [advMK, calc1MK],
    };
    let args = Cli::from_args();
    let cmd = std::env::args().nth(1).expect("no command given");
    let pattern = std::env::args().nth(2).expect("no pattern given");
    let mut i = 0;
    while i < 2 {
        if cmd == String::from("get") && pattern == fs.cont[i].id {
            let tcmd = Command::new("wget")
                .arg(&fs.cont[i].url)
                .output()
                .expect("failed to execute process");
            if tcmd.status.success() == true {
                println!("Installed Successfully!")
            }
        }
        i += 1;
    }
    // List commands
if cmd == String::from("list") && pattern == String::from("--fs") {
        List(fs);
    }
}
