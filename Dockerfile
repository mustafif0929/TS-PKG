FROM ubuntu
FROM rust
COPY ts-pkg.deb /bin/
RUN apt-get update
RUN  apt-get install build-essential -y
RUN apt-get install libc6 
RUN apt-get update -y && apt-get upgrade -y 
RUN dpkg -i /bin/ts-pkg.deb
CMD [ "ts-pkg", "subcommand"]