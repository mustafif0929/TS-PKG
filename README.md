# Tomorrow Study Package Manager 

The Tomorrow Study Packager Manager will be made to be available on the following 
platforms: 
- Snapstore 
- Docker

It is recommended to use this manager in a Linux Distro, and if you want 
it to work on Windows 10 or Mac OS, then you may need to change the system commands 
to match the one's for your appropriate system. 
